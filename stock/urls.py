from django.conf.urls import patterns, include, url
from django.contrib import admin
import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tile.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #product quantities
    url(r'^(?P<warehouse_id>[a-zA-Z0-9]+)/quantities', views.quantities, name = 'stocks_quantities'),
    url(r'^(?P<warehouse_id>[a-zA-Z0-9]+)/delivery_time', views.delivery_time, name = 'delivery_time'),
)


