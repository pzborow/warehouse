from django.db import models
from common.models import Warehouse

class Stock(models.Model):
    warehouse = models.ForeignKey(Warehouse, null=False, related_name='stocks')
    product = models.CharField(max_length=16, null=False)
    quantity = models.IntegerField(null=False, default=0)
    class Meta:
        unique_together = ( ('warehouse', 'product'), )
