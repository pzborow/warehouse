from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponse, Http404 #temporary
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext
import common.decorators.model_retrievers as retrievers
from common.models import Warehouse
from stock.models import Stock

@retrievers.warehouse_retriever
@csrf_protect
def quantities(request, warehouse):
    context = {
        'warehouse': warehouse,
        'warehouses': Warehouse.objects.all(),
        'products': [str(p) for p in Stock.objects.values_list('product', flat=True).distinct()]
    }
    return render(request, 'stocks_delivery/query.html', context)

@retrievers.warehouse_retriever
@csrf_protect
def delivery_time(request, warehouse):
    products_quantities = request.POST['products_quantities']
    if not products_quantities:
        return HttpResponse('Select something')
        
    products_quantities = [pd.split(':') for pd in products_quantities.split(',')]
    pqs = {}
    for pq in products_quantities:
        if pq[0] not in pqs:
            pqs[pq[0]] = 0
        pqs[pq[0]] += int(pq[1])
    products_quantities = pqs

    time = warehouse.products_deliveries( products_quantities )
    if time == float('inf'):
        time = ugettext("Delivery is impossible, to many")
    else:
        time = ugettext('Delivery time: %d') % time
    return HttpResponse(time)
