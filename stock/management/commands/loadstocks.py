import os
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from stock.models import Stock
from common.models import Warehouse, Connection

class Command(BaseCommand):
    help = "Load/ reload stocks in warehouses"
    option_list = BaseCommand.option_list + (
        make_option('--force',
            action="store_true",
            help="Without this option nothing heppens",
            default=False
        ),
        make_option('--stocks-file', '-s',
            help='CSV file containng columns: warehouse, product, quantity, omits first row as header'
        ),
        make_option('-a', '--action',
            type='choice',
            help="Choices are: replace or update, Replace will delete all stocks and load again",
            choices=('replace', 'update')
        )
    )

    @transaction.atomic
    def handle(self, **options):
        if not options['force']:
            self.stderr.write(
                self.style.ERROR('Add force to perform delete')
            )
            return

        if not options['stocks_file']:
            self.stderr.write(
                self.style.ERROR("No file path specified. Please provide the path of file with stocks\n")
            )
            return

        if not os.path.exists(options['stocks_file']):
            self.stderr.write(
                self.style.ERROR("Specified file not found")
            )
            return

        print 'file', options['stocks_file']

        if options['action'] == 'replace':
            Stock.objects.all().delete()

        with open(options['stocks_file'], 'r') as f:
            f.readline()
            for l in f:
                l = [c.strip() for c in l.strip().split(',')]
                w = Warehouse.objects.get(symbol = l[0])

                #TODO handle exceptions, such no warehouse, quantity convertion
                s = Stock( warehouse = w, product = l[1], quantity = int(l[2]) )
                s.save()


