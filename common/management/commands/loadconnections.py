import os
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction
from common.models import Warehouse, Connection

class Command(BaseCommand):
    help = "Reload all warehouse'es connections"
    option_list = BaseCommand.option_list + (
        make_option('--force',
            action="store_true",
            help="Without this option nothing heppens",
            default=False
        ),
        make_option('--connections-file', '-c', dest='connections_file',
            help='CSV file containng columns: warehouse1, warehouse2, time, omits first row as header'
        ),
        make_option('--auto-add-warehouse',
            action="store_true",
            help="With this option if warehouse not exists, then creates it, else omit",
            default=False
        ),
    )

    def do_find_warehouse(self, warehouse, options):
        if options['auto_add_warehouse']:
            w, created = Warehouse.objects.get_or_create(symbol = warehouse)
        else:
            w = Warehouse.objects.get(symbol = warehouse)
        return w
        

    @transaction.atomic
    def handle(self, **options):
        if not options['force']:
            self.stderr.write(
                self.style.ERROR('Add force to perform delete')
            )
            return

        if not options['connections_file']:
            self.stderr.write(
                self.style.ERROR("No file path specified. Please provide the path of file with connections\n")
            )
            return

        if not os.path.exists(options['connections_file']):
            self.stderr.write(
                self.style.ERROR("Specified file not found")
            )
            return

        print 'file', options['connections_file']

        Connection.objects.all().delete()
        with open(options['connections_file'], 'r') as f:
            f.readline()
            for l in f:
                l = [c.strip() for c in l.strip().split(',')]
                w1 = self.do_find_warehouse(l[0], options)
                w2 = self.do_find_warehouse(l[1], options)

                #TODO handle exceptions, such duplication, time convertion
                c = Connection( warehouse1 = w1, warehouse2 = w2, time = int(l[2]) )
                c.save()
