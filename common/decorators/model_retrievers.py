from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from common.models import Warehouse

def warehouse_retriever(func):
    def inner(request, warehouse_id, *args, **kwargs):
        warehouse = get_object_or_404(Warehouse, symbol=warehouse_id)
        return func(request, warehouse, *args, **kwargs)
    return inner
