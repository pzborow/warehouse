from django.test import TestCase
from common.utils.dijkstra import Graph

class TestDijkstra(TestCase):
    def test_shortest_path(self):
        graph = Graph([("a", "b", 7),  ("a", "c", 9),  ("a", "f", 14), ("b", "c", 10),
                       ("b", "d", 15), ("c", "d", 11), ("c", "f", 2),  ("d", "e", 6),
                       ("e", "f", 9)])
        self.assertEqual(graph.dijkstra("a")['e']['path'], ['a', 'c', 'd', 'e'])
        self.assertEqual(graph.dijkstra("a")['e']['cost'], 26)
