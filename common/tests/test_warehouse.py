from django.test import TestCase
from common.utils.dijkstra import Graph
from common.models import Warehouse

class TestDijkstra(TestCase):
    fixtures = ['fixtures/test_connections.json', 'fixtures/test_stocks.json']

    def warehouse(self, symbol):
        return Warehouse.objects.get(symbol = symbol)

    def test_connections(self):
        dd = self.warehouse('A').find_distances()
        self.assertDictEqual(dd[0], 
            {'dest': self.warehouse('B'), 'path': [self.warehouse('A'), self.warehouse('B')], 'time': 7}
        )
        self.assertTrue(repr(dd[4]), 
            {'dest': self.warehouse('E'), 'path': [self.warehouse('A'), self.warehouse('C'), self.warehouse('D'), self.warehouse('E')], 'time': 26}
        )

    def test_stocks_deliver_for_one_product(self):
        w = self.warehouse('A')
        self.assertListEqual( w.product_deliveries('p1', 0), [] )
        self.assertListEqual( w.product_deliveries('p1', 5),
            [{'quantity': 5, 'from': w , 'time': 0}] 
        )

        self.assertListEqual(w.product_deliveries('p1', 6), 
            [
                {'quantity': 5, 'from': w , 'time': 0},
                {'quantity': 1, 'from': self.warehouse('B') , 'time': 7, 'via': []}
            ] 
        )

        self.assertListEqual(w.product_deliveries('p1', 10), 
            [
                {'quantity': 5, 'from': w , 'time': 0},
                {'quantity': 5, 'from': self.warehouse('B') , 'time': 7, 'via': []}
            ] 
        )

        self.assertListEqual(w.product_deliveries('p1', 11), #path should be A->C->D->E
            [
                {'quantity': 5, 'from': w , 'time': 0},
                {'quantity': 5, 'from': self.warehouse('B') , 'time': 7, 'via': []},
                {
                    'quantity': 1, 'from': self.warehouse('E'),
                    'via': [self.warehouse('C'), self.warehouse('D')],
                    'time': 26
                }
            ] 
        )

        self.assertEqual(w.product_deliveries('p1', 16), None) #there isn't enough


    def test_stocks_deliver_for_one_product(self):
        w = self.warehouse('A')

        self.assertEqual( w.products_deliveries({'p1': 5}), 0)
        self.assertEqual( w.products_deliveries({'p1': 6}), 7)
        self.assertEqual( w.products_deliveries({'p2': 5}), 20)
        self.assertEqual( w.products_deliveries({'p2': 6}), float('inf'))
        self.assertEqual( w.products_deliveries({'p1': 11, 'p2': 5}), 26)
        self.assertEqual( w.products_deliveries({'p1': 11, 'p2': 6}), float('inf'))
        
