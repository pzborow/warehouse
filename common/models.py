from django.db import models
from django.db.models import Q
from common.utils.dijkstra import Graph
from operator import itemgetter
from django.core.exceptions import ObjectDoesNotExist

class Warehouse(models.Model):
    symbol = models.CharField(max_length=16, db_index=True, unique=True, blank=False)

    #def neighbours(self):
    #    return Connection.objects.filter(Q(warehouse1 = self) | Q(warehouse2 = self))

    def find_distances(self):
        ww = {}
        g = []
        for c in Connection.objects.all():
            g.append( (c.warehouse1.symbol, c.warehouse2.symbol, c.time) )
            ww[c.warehouse1.symbol] = c.warehouse1
            ww[c.warehouse2.symbol] = c.warehouse2

        g = Graph(g)
        paths = g.dijkstra(self.symbol)
        paths = [
            {
                'dest': ww[w],
                'path': [ww[p] for p in path['path']],
                'time': path['cost']
            } for w, path in paths.iteritems()
        ]
        paths = sorted(paths, key=itemgetter('time'))
        
        return paths

    def __repr__(self):
        return "<%(class)s: %(caption)s>" % {
            'class': self.__class__.__name__,
            'caption': self.symbol.encode('unicode-escape')
        } 

    def product_quantity(self, product):
        try:
            self_stocks = self.stocks.get(product=product)
            return self_stocks.quantity
        except ObjectDoesNotExist:
            return 0

    def product_deliveries(self, product, needed_quantity):
        deliveries = []
        still_needed_quantity = needed_quantity

        #first take from self
        q = min(self.product_quantity(product), still_needed_quantity)
        if q > 0:
            deliveries.append({'time': 0, 'from': self, 'quantity': q})
            still_needed_quantity -= q

        #if no more is needed then return
        if still_needed_quantity <= 0:
            return deliveries

        #if more are needed then collect from other warehouses, in order of shortest time
        paths = self.find_distances()
        for path in paths:
            #take as much as you can
            q = min(path['dest'].product_quantity(product), still_needed_quantity)
            
            if q > 0:
                still_needed_quantity -= q
                via = [w for w in path['path'] if w != path['dest'] and w != self]
                deliveries.append( {'from': path['dest'], 'via': via, 'time': path['time'], 'quantity': q} )

            #enough?
            if still_needed_quantity <= 0:
                return deliveries
        
        #if still_needed_quantity > 0:
        #not good, there arent enough
        return None

    def products_deliveries(self, products_quantities, details = False):
        deliveries = {}
        inf = float('inf')
        for product, quantity in products_quantities.iteritems():
            deliveries[product] = self.product_deliveries(product, quantity)

        if details:
            return deliveries
        else:
            return max([
                max(  [ delivery['time'] for delivery in deliveries or [{'time': inf}] ]  )
                for product, deliveries in deliveries.iteritems()
            ])
            

class Connection(models.Model):
    warehouse1 = models.ForeignKey(Warehouse, null=False, related_name='warehouses1')
    warehouse2 = models.ForeignKey(Warehouse, null=False, related_name='warehouses2')
    time = models.IntegerField(null=False)

    class Meta:
        unique_together = (('warehouse1' ,'warehouse2'), )
