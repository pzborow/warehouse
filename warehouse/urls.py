from django.conf.urls import patterns, include, url
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
#from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'warehouse.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #url(r'^admin/', include(admin.site.urls)),
    url(r'^stocks/', include('stock.urls')),
    url(r'^$', lambda r: redirect(reverse('stocks_quantities', kwargs={'warehouse_id': 'C1'}), permanent=True), {}),
)
