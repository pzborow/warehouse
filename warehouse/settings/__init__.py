import os
from base import *

ENVIRONMENT = os.getenv("WAREHOUSE_ENV") or 'staging'

if ENVIRONMENT == 'live':
    from live import *
elif ENVIRONMENT == 'staging':
    from staging import *
print "Used " + ENVIRONMENT

try:
    from local import *
except ImportError:
    pass
