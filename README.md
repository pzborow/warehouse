# Description

This is sample project written in Python and Django. For given warhouse and list of needed products, applications searches in 
network of warehouses needed products and calculate minimum time of delivery. Warhouses has weighted links (which represents time of traveling)
between them and stock status.

To solve this, application uses Dijkstra's algoritm to find closest path between nodes in a graph, which may represent,
for example, road networks.


# Installation of Warehouse

Install python packages

    :::bash
    pip install -r requirements.txt

Copy settings, base on example, and setup

    :::bash
    cp warehouse/settings/staging.py.example warehouse/settings/staging.py

Edit warehouse/settings/staging.py and setup database connection

Next create all tables
    
    :::bash
    ./manage.py syncdb

Load example warehouses connections (due to ownership of connections.csv file is not included)

    :::bash
    ./manage.py loadconnections --connections-file=fixtures/provided_data/connections.csv --force

Format connections file should be CSV separated by ',', example

warehouse1|warehouse2|time
----|----|----
wh1|wh2|3
wh2|wh5|1

Load example stocks (due to ownership of stocks.csv file is not included)

    :::bash
        ./manage.py loadstocks --stocks-file=fixtures/provided_data/stocks.csv --force --action=replace

Format stocks file should be CSV separated by ',', example

warehouse|product|quantity
----|----|----
wh1|p1|10
wh2|p2|1